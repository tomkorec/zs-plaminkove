<?php
/**
 * Header file
 */

?><!DOCTYPE html>

<html <?php language_attributes(); ?>>

<head>

	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>

	<title><?php wp_title(''); ?></title>

</head>

<body <?php body_class(); ?>>
<div id="header" class="<?php echo is_home() ? 'homepage' : ''; ?>">
	<!--	<div class="toolbar">-->
	<!--		<div class="container">-->
	<!--			<div class="toolbar-content">-->
	<!--				<div class="toolbar-content-left"></div>-->
	<!--				<div class="toolbar-content-right">-->
	<!--					<nav id="navigation" class="navbar">-->
	<!--						--><?php //wp_nav_menu(['theme_location' => 'toolbar-menu']); ?>
	<!--					</nav>-->
	<!--				</div>-->
	<!--			</div>-->
	<!--		</div>-->
	<!--	</div>-->
	<div class="navigation">
		<div class="logo-content">
			<a href="<?php echo home_url() ?>" class="logo">
				<img src="<?php basePath() ?>/src/img/logo.svg" alt="logo školy"/>
			</a>
		</div>
		<div class="hamburger">☰</div>
		<div class="header-content">
			<nav id="navigation" class="navbar">
				<?php wp_nav_menu(['theme_location' => 'navbar-menu']); ?>
			</nav>
		</div>
	</div>
</div>
<main class="<?php echo is_home() ? 'homepage' : ''; ?>">

	<?php
	wp_body_open();
	?>
