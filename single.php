<?php
/*
Template Name: Single
*/
get_header(); ?>
<section class="page">
	<div class="container">
		<?php

		// Start the Loop.
		while (have_posts()) :
			the_post();

			get_template_part('template-parts/content/content', 'single');

		endwhile; // End the loop.
		?>
	</div>

</section>

<?php get_footer(); ?>
