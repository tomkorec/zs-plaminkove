<?php
/**
 * The template for displaying the footer
 */

?>

</main>

<div class="scroll-to-top"><span class="icon-arrow-up"></span></div>
<footer>
	<div class="container">
		<div class="footer-wrapper">
			<div class="footer-column">
				<img src="<?php basePath() ?>/src/img/logo-white.svg" alt="světlé logo" class="img-shady"/>
			</div>
			<div class="footer-column">
				<?php wp_nav_menu(['theme_location' => 'footer-1']); ?>
			</div>
			<div class="footer-column">
				<?php wp_nav_menu(['theme_location' => 'footer-2']); ?>
			</div>
			<div class="footer-column">
				<span>&copy; H.S.H. Computer s.r.o.</span>
			</div>
		</div>
	</div>
</footer>

<?php wp_footer(); ?>
<script src="<?php echo get_template_directory_uri() ?>/dist/main.js?v=<?php echo mt_rand(1, 135); ?>"></script>
</body>
</html>
