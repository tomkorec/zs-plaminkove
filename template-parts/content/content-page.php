<?php
$high = get_field('high_featured_image', get_the_ID()) ? 'high' : '';
?>

<div class="page-content">
	<div class="header <?php echo has_post_thumbnail() ? '' : 'blank ';
	echo $high; ?>">
		<div class="header-background">
			<?php the_post_thumbnail(); ?>
		</div>
		<?php the_title('<h1 class="title">', '</h1>'); ?>
	</div>
	<div class="text">
		<?php
		the_content();
		?>
	</div>

</div>
