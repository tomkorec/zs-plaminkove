<div class="blog-post">
	<div class="blog-post-header">
		<?php the_title('<h1 class="title">', '</h1>'); ?>
		<div class="details">
			<?php the_time(get_option('date_format')); ?>
		</div>
	</div>
	<div class="blog-post-content">
		<?php
		the_content(
			sprintf(
				get_the_title()
			)
		);
		?>
	</div>
</div>
