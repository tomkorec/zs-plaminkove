<div class="gallery">
	<?php the_title('<h1 class="title">', '</h1>'); ?>
	<div class="gallery-tiles">
		<?php
		the_content();
		?>
	</div>
</div>
