<div class="coop">
	<div class="container">
		<h2>Spolupracují s námi</h2>
		<div class="coop-reel">
			<a href="https://www.dbkpraha.cz/">
				<img src="<?php basePath() ?>/src/img/coop/dbk.png" alt="DBK Praha">
			</a>
			<a href="http://www.slavojvysehrad.cz/">
				<img src="<?php basePath() ?>/src/img/coop/slavoj.png" alt="FC Slavoj Vyšehrad">
			</a>
			<a href="https://sokolvysehrad.webnode.cz/">
				<img src="<?php basePath() ?>/src/img/coop/sokol.jpg" alt="FC Basketbalový klub Sokol Vyšehrad">
			</a>
			<a href="http://www.euroinstitut.cz/">
				<img src="<?php basePath() ?>/src/img/coop/euroinstitut.jpg" alt="EUROINSTITUT | vzděláváním proti handicapu">
			</a>
			<a href="http://www.4pastelky.cz/">
				<img src="<?php basePath() ?>/src/img/coop/4pastelky.png" alt="MŠ 4 pastelky">
			</a>
			<a href="http://www.danceplaminek.cz/">
				<img src="<?php basePath() ?>/src/img/coop/modern_dance.png" alt="Modern dance Plamínek">
			</a>
		</div>
	</div>
</div>
