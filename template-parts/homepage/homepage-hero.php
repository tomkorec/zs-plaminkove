<div class="hero">
	<div class="waves">
		<div class="topwave">
			<img src="<?php basePath() ?>/src/img/waves/hero-topleft.svg"/>
		</div>
		<!--		<div class="bottom-wave">-->
		<!--			<img src="--><?php //basePath() ?><!--/src/img/waves/hero-bottom.svg"/>-->
		<!--		</div>-->
	</div>
	<div class="container">
		<div class="message">
			<h1 class="hero-huge">ZŠ Plamínkové 2</h1>
			<h2 class="hero-subtitle">Míříme vysoko</h2>
		</div>
	</div>
	<div class="hero-image">
		<div class="glide glide-hero">
			<div class="glide__track" data-glide-el="track">
				<ul class="glide__slides">
					<li class="glide__slide">
						<div class="hero-image-container">
							<img src="<?php basePath() ?>/src/img/samples/hero-sample-1.jpg" alt="Učebna"/>
						</div>
					</li>
					<li class="glide__slide">
						<div class="hero-image-container">
							<img src="<?php basePath() ?>/src/img/samples/hero-sample-2.jpg" alt="Další učebna"/>
						</div>
					</li>
					<li class="glide__slide">
						<div class="hero-image-container">
							<img src="<?php basePath() ?>/src/img/samples/hero-sample-3.jpg" alt="Herna"/>
						</div>
					</li>
					<li class="glide__slide">
						<div class="hero-image-container">
							<img src="<?php basePath() ?>/src/img/samples/hero-sample-4.jpg" alt="Tělocvična"/>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
