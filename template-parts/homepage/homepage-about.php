<div class="about">
	<div class="container">
		<div class="about-inner-wrapper">
			<div class="card">
				<h2>O škole</h2>
				<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nulla non arcu lacinia neque faucibus fringilla. Quisque porta. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam
					eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.
					Maecenas
					lorem.

				</p>
			</div>
			<div class="features">
				<a href="o-skole#tradice" class="btn red">Tradice
					<div class="bg"></div>
				</a>
				<a href="o-skole#vzdelani" class="btn blue">Dobré základní vzdělání
					<div class="bg"></div>
				</a>
				<a href="o-skole#tym" class="btn green">Vyvážený tým odborníků
					<div class="bg"></div>
				</a>
				<a href="o-skole#podminky" class="btn purple">Nadstandardní podmínky
					<div class="bg"></div>
				</a>
			</div>

		</div>
	</div>
</div>

