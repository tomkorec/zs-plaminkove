<div class="contact">
	<h2>Kontakt</h2>
	<div class="contact-wrapper">
		<div class="contact-background">
			<div class="map">
				<a href="https://goo.gl/maps/cD2BJkqZwkwgzScA9">
					<img src="<?php basePath() ?>/src/img/map.png" alt="mapa"/>
				</a>
			</div>
		</div>
		<div class="contact-card-anchor">
			<div class="contact-card">
				<h3>Adresa</h3>
				<address>
					Základní škola<br>
					Plamínkové 2/1593<br>
					140 00 Praha 4
				</address>
				<h3>E-mail</h3>
				<span>info@zsplaminkove.cz</span>
			</div>
			<a class="contact-button" href="/kontakt">DALŠÍ KONTAKTY</a>

		</div>

	</div>
</div>
