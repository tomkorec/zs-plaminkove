<div class="news">
	<h2>AKTUALITY</h2>
	<?php if (have_posts()) : ?>
		<div class="container">
			<div class="glide glide-news">
				<div class="controls">
					<div class="glide__arrows" data-glide-el="controls">
						<span class="glide__arrow glide__arrow--left icon-chevron-left" data-glide-dir="<"></span>
						<span class="glide__arrow glide__arrow--right icon-chevron-right" data-glide-dir=">"></span>
					</div>
				</div>
				<div class="glide__track" data-glide-el="track">
					<ul class="glide__slides">
						<?php while (have_posts()) : the_post(); ?>
							<li class="glide__slide">
								<div class="news-item">
									<!--									<div class="news-item-image --><?php //if (get_the_post_thumbnail()) : ?><!--thumbnail--><?php //endif; ?><!--">-->
									<!--										--><?php //echo get_the_post_thumbnail(); ?>
									<!--									</div>-->
									<div class="news-item-content">
										<div class="title-and-date">
											<a href="<?php the_permalink(); ?>">
												<?php the_title('<h3>', '</h3>'); ?>
											</a>
											<div class="date"><?php the_time(get_option('date_format')); ?></div>
										</div>
										<p><?php
											echo mb_strimwidth(strip_tags(get_the_content()), 0, 250, '...');
											?>
										</p>
										<a href="<?php the_permalink(); ?>" class="readmore">Celý článek →</a>
									</div>
								</div>
							</li>
						<?php endwhile; ?>
					</ul>
				</div>
			</div>
		</div>
	<?php endif ?>
</div>


