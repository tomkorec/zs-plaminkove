<?php
/*
Template Name: Home
*/
get_header(); ?>

<section id="hero">
	<?php get_template_part('template-parts/homepage/homepage', 'hero'); ?>
</section>
<section id="features">
	<?php get_template_part('template-parts/homepage/homepage', 'features'); ?>
</section>
<section id="news">
	<?php get_template_part('template-parts/homepage/homepage', 'news'); ?>
</section>
<section id="about">
	<?php get_template_part('template-parts/homepage/homepage', 'about'); ?>
</section>
<section id="cooperation">
	<?php get_template_part('template-parts/homepage/homepage', 'cooperation'); ?>
</section>
<section id="gallery">
	<?php get_template_part('template-parts/homepage/homepage', 'gallery'); ?>
</section>
<section id="contact">
	<?php get_template_part('template-parts/homepage/homepage', 'contact'); ?>
</section>
<div data-name="homePage"></div>
<?php get_footer(); ?>

