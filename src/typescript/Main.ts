import {FadeIn} from "./homepage/FadeIn";
import {Navbar} from "./navigation/Navbar";
import {Page} from "./page/Page";
import {ScrollToTop} from "./scrolling/ScrollToTop";

export class Main {

	constructor() {
		if (Main.isCurrentPage('homePage')) {
			new FadeIn();
		}
		if (!!document.querySelector('.hamburger')) {
			new Navbar();
		}

		if (!!document.querySelector('footer')) {
			new ScrollToTop();
		}

		if (Main.isCurrentPage('page')) {
			new Page();
		}

	}

	private static isCurrentPage(pageName: string): boolean {
		return !!document.querySelector('[data-name="' + pageName + '"]');
	}

}
