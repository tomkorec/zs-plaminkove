import {Subject} from "rxjs";
import {throttleTime} from "rxjs/operators";
import {ScrollAgent} from "../helpers/ScrollAgent";

export class FadeIn {
	private scroll$ = new Subject<void>();
	private fadingElements: NodeListOf<HTMLElement>;

	constructor() {
		this.fadingElements = document.querySelectorAll('.fading');

		this.bindEventListeners();

		this.scroll$
			.pipe(
				throttleTime(300),
			).subscribe(_ => {
			this.revealFadingElements();
		});
	}

	private bindEventListeners() {

		document.addEventListener('scroll', e => {
			this.scroll$.next();
		});

	}

	private revealFadingElements() {
		for (let el of this.fadingElements) {
			if (ScrollAgent.isElementInViewport(el)) {
				console.log('inView');
				el.classList.add('faded');
			} else {
				console.log('out of vir');
			}
		}
	}
}
