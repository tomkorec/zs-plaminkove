import {Subject} from "rxjs";

export class ScrollToTop {
	private readonly scrollToTopButton: any;
	private scroll$ = new Subject<number>();

	constructor() {
		this.scrollToTopButton = document.querySelector('.scroll-to-top');

		if (!this.scrollToTopButton) {
			return;
		}

		this.bindEventListeners();
	}

	private bindEventListeners() {


		this.scrollToTopButton.addEventListener('click', e => {
			document.querySelector('#header').scrollIntoView({behavior: "smooth"});
		});

	}
}
