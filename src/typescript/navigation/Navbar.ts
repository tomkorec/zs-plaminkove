import {combineLatest, fromEvent, Subject} from "rxjs";
import {filter, first} from "rxjs/operators";

export class Navbar {
	private hamburger: HTMLElement;
	private navbarElement: HTMLElement;
	private submenuElements: NodeListOf<HTMLElement>;

	private isMobileNavbarOpened: boolean = false;
	private mobileNavbar$ = new Subject<boolean>();

	constructor() {
		this.navbarElement = document.querySelector('.navbar');
		this.submenuElements = document.querySelectorAll('#header .navbar .sub-menu');
		this.hamburger = document.querySelector('.hamburger');

		this.bindEventListeners();
	}


	private bindEventListeners() {
		this.hamburger.addEventListener('click', e => {
			this.isMobileNavbarOpened = !this.isMobileNavbarOpened;
			this.mobileNavbar$.next(this.isMobileNavbarOpened);
		});

		this.mobileNavbar$
			.asObservable()
			.subscribe(isOpened => {
				if (isOpened) {
					this.navbarElement.classList.add('opened');
				} else {
					this.navbarElement.classList.remove('opened');
				}
			});

		for (let submenu of this.submenuElements) {
			let parentLink = submenu.parentElement.querySelector('a');

			let unwrapper = document.createElement('div');
			unwrapper.classList.add('unwrapper');
			parentLink.parentNode.insertBefore(unwrapper, parentLink.nextSibling);


			unwrapper.addEventListener('click', e => {
				e.preventDefault();
				submenu.classList.toggle('active');
				unwrapper.classList.toggle('active');
			});

			parentLink.addEventListener('mouseenter', e => {
				submenu.classList.add('active');

				combineLatest([
					fromEvent(submenu, 'mouseleave'),
					fromEvent(parentLink, 'mouseleave')
				]).pipe(
					first()
				).subscribe(e => {
					submenu.classList.remove('active');
				});

			});
		}
	}

}
