export class Page {

	constructor() {
		this.bindEventListeners();
	}


	private bindEventListeners() {
		let headers = document.querySelectorAll('h2,h3');

		for (let header of headers) {
			header.addEventListener('click', e => {
				header.scrollIntoView({behavior: "smooth"});
				//Todo: implement behaviour for ids
			});
		}
	}
}
