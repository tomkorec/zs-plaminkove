/* I. styles import */
import './styles/main.styl';

import '@glidejs/glide/dist/css/glide.core.min.css';

import Glide from '@glidejs/glide';
import $ from 'jquery';
import {Main} from "./typescript/Main";
import ArrowDisabler from "./js/ArrowDisabler";

new Main();

$(document).ready(function () {
	const glideHeroOptions = {
		type: 'carousel',
		startAt: 1,
		perView: 1,
		keyboard: false,
		autoplay: 7000,
		animationDuration: 1000,
	};

	const glideNewsOptions = {
		type: 'slider',
		startAt: 0,
		perView: 2,
		rewind: false,
		breakpoints: {
			800: {
				perView: 1
			}
		}
	};

	if ($('.glide-hero').length) {
		new Glide('.glide-hero', glideHeroOptions).mount();
	}

	if ($('.glide-news').length) {
		new Glide('.glide-news', glideNewsOptions).mount({ArrowDisabler});
	}


});

