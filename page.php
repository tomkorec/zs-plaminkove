<?php
/*
Template Name: Page
*/
get_header(); ?>
<section class="page">
	<div class="container">
		<?php

		// Start the Loop.
		while (have_posts()) :
			the_post();

			get_template_part('template-parts/content/content', 'page');

		endwhile; // End the loop.
		?>
	</div>
</section>
<div data-name="page"></div>
<?php get_footer(); ?>

