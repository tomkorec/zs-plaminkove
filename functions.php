<?php


/**
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 */

/**
 * Enqueue frontend scripts.
 */

//wp_enqueue_script('script', get_template_directory_uri() . '/dist/main.js');

function enqueue_script()
{
	wp_enqueue_script('main-js', get_template_directory_uri() . '/dist/main.js', false);
}

add_action('wp_enqueue_scripts', 'enqueue_script');

function basePath()
{
	echo get_template_directory_uri();
}

add_theme_support('post-thumbnails');

function addNavbarMenu()
{
	register_nav_menus(
		array(
			'navbar-menu' => __('Navbar menu'),
			'toolbar-menu' => __('Toolbar menu'),
			'footer-1' => __('Footer left'),
			'footer-2' => __('Footer right'),
		));
}

add_action('init', 'addNavbarMenu');
